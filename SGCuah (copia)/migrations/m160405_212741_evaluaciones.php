<?php

use yii\db\Migration;

class m160405_212741_evaluaciones extends Migration
{
    public function up()
    {
        $this->createTable('evaluaciones', [
            'id' => $this->primaryKey(),
            'fk_sistema' => $this->integer(),

            'funcionalidad' => $this->integer()->notNull(),
            'fu_adecuacion' => $this->integer()->notNull(),
            'fu_exactitud' => $this->integer()->notNull(),
            'fu_interoperabil' => $this->integer()->notNull(),
            'fu_conformidad' => $this->integer()->notNull(),
            'confiabilidad' => $this->integer()->notNull(),
            'co_madurez' => $this->integer()->notNull(),
            'co_tol_error' => $this->integer()->notNull(),
            'co_recuperabil' => $this->integer()->notNull(),
            'usabilidad' => $this->integer()->notNull(),
            'us_entendimiento' => $this->integer()->notNull(),
            'us_aprendizaje' => $this->integer()->notNull(),
            'us_operabilidad' => $this->integer()->notNull(),
            'us_atraccion' => $this->integer()->notNull(),
            'eficiencia' => $this->integer()->notNull(),
            'efi_comport_tiemp' => $this->integer()->notNull(),
            'efi_util_recur' => $this->integer()->notNull(),
            'cap_manten' => $this->integer()->notNull(),
            'cm_cap_analiz' => $this->integer()->notNull(),
            'cm_cambialidad' => $this->integer()->notNull(),
            'cm_estabilidad' => $this->integer()->notNull(),
            'cm_fac_prueba' => $this->integer()->notNull(),
            'portabilidad' => $this->integer()->notNull(),
            'po_adaptabilidad' => $this->integer()->notNull(),
            'po_fac_instal' => $this->integer()->notNull(),
            'po_reemplazabi' => $this->integer()->notNull(),
            'po_coexistencia' => $this->integer()->notNull(),
            'cal_enuso' => $this->integer()->notNull(),
            'c_uso_eficacia' => $this->integer()->notNull(),
            'c_uso_productividad' => $this->integer()->notNull(),
            'c_uso_seguridad' => $this->integer()->notNull(),
            'calidadTotal' => $this->integer()->notNull(),
            'errorFound' => $this->integer(),
            'errorCorrect' => $this->integer(),
            'errorDescrip' => $this->text()
        ]);
        $this->addForeignKey('fk_sistema','evaluaciones','fk_sistema','sistemas','id','CASCADE');

    }

    public function down()
    {
        /*echo "m160405_183221_analistas cannot be reverted.\n";

        return false;*/
        $this->dropTable('evaluaciones');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m160405_212709_sistemas extends Migration
{
    public function up()
    {
        $this->createTable('sistemas', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'descripcion' => $this->text(),
            'fk_analista' => $this->integer()
        ]);
        $this->addForeignKey('fk_analista','sistemas','fk_analista','analistas','id','CASCADE');

    }

    public function down()
    {
        /*echo "m160405_183221_analistas cannot be reverted.\n";

        return false;*/
        $this->dropTable('sistemas');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

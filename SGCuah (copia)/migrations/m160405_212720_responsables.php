<?php

use yii\db\Migration;

class m160405_212720_responsables extends Migration
{
    public function up()
    {
        $this->createTable('responsables', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'cedula' => $this->string(8),
            'fk_sistema' => $this->integer()
        ]);
        $this->addForeignKey('fk_sistema','responsables','fk_sistema','sistemas','id','CASCADE');

    }

    public function down()
    {
        /*echo "m160405_183221_analistas cannot be reverted.\n";

        return false;*/
        $this->dropTable('responsables');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

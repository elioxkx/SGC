<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "responsables".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $cedula
 * @property integer $fk_sistema
 *
 * @property Sistemas $fkSistema
 */
class Responsables extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'responsables';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['fk_sistema'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['cedula'], 'string', 'max' => 8],
            [['fk_sistema'], 'exist', 'skipOnError' => true, 'targetClass' => Sistemas::className(), 'targetAttribute' => ['fk_sistema' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'cedula' => 'Cedula',
            'fk_sistema' => 'Fk Sistema',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkSistema()
    {
        return $this->hasOne(Sistemas::className(), ['id' => 'fk_sistema']);
    }
}

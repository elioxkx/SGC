<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Evaluaciones;

/**
 * EvaluacionesSearch represents the model behind the search form about `app\models\Evaluaciones`.
 */
class EvaluacionesSearch extends Evaluaciones
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fk_sistema', 'funcionalidad', 'fu_adecuacion', 'fu_exactitud', 'fu_interoperabil', 'fu_conformidad', 'confiabilidad', 'co_madurez', 'co_tol_error', 'co_recuperabil', 'usabilidad', 'us_entendimiento', 'us_aprendizaje', 'us_operabilidad', 'us_atraccion', 'eficiencia', 'efi_comport_tiemp', 'efi_util_recur', 'cap_manten', 'cm_cap_analiz', 'cm_cambialidad', 'cm_estabilidad', 'cm_fac_prueba', 'portabilidad', 'po_adaptabilidad', 'po_fac_instal', 'po_reemplazabi', 'po_coexistencia', 'cal_enuso', 'c_uso_eficacia', 'c_uso_productividad', 'c_uso_seguridad', 'calidadTotal', 'errorFound', 'errorCorrect'], 'integer'],
            [['errorDescrip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Evaluaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fk_sistema' => $this->fk_sistema,
            'funcionalidad' => $this->funcionalidad,
            'fu_adecuacion' => $this->fu_adecuacion,
            'fu_exactitud' => $this->fu_exactitud,
            'fu_interoperabil' => $this->fu_interoperabil,
            'fu_conformidad' => $this->fu_conformidad,
            'confiabilidad' => $this->confiabilidad,
            'co_madurez' => $this->co_madurez,
            'co_tol_error' => $this->co_tol_error,
            'co_recuperabil' => $this->co_recuperabil,
            'usabilidad' => $this->usabilidad,
            'us_entendimiento' => $this->us_entendimiento,
            'us_aprendizaje' => $this->us_aprendizaje,
            'us_operabilidad' => $this->us_operabilidad,
            'us_atraccion' => $this->us_atraccion,
            'eficiencia' => $this->eficiencia,
            'efi_comport_tiemp' => $this->efi_comport_tiemp,
            'efi_util_recur' => $this->efi_util_recur,
            'cap_manten' => $this->cap_manten,
            'cm_cap_analiz' => $this->cm_cap_analiz,
            'cm_cambialidad' => $this->cm_cambialidad,
            'cm_estabilidad' => $this->cm_estabilidad,
            'cm_fac_prueba' => $this->cm_fac_prueba,
            'portabilidad' => $this->portabilidad,
            'po_adaptabilidad' => $this->po_adaptabilidad,
            'po_fac_instal' => $this->po_fac_instal,
            'po_reemplazabi' => $this->po_reemplazabi,
            'po_coexistencia' => $this->po_coexistencia,
            'cal_enuso' => $this->cal_enuso,
            'c_uso_eficacia' => $this->c_uso_eficacia,
            'c_uso_productividad' => $this->c_uso_productividad,
            'c_uso_seguridad' => $this->c_uso_seguridad,
            'calidadTotal' => $this->calidadTotal,
            'errorFound' => $this->errorFound,
            'errorCorrect' => $this->errorCorrect,
        ]);

        $query->andFilterWhere(['like', 'errorDescrip', $this->errorDescrip]);

        return $dataProvider;
    }
}

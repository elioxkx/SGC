<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evaluaciones".
 *
 * @property integer $id
 * @property integer $fk_sistema
 * @property integer $funcionalidad
 * @property integer $fu_adecuacion
 * @property integer $fu_exactitud
 * @property integer $fu_interoperabil
 * @property integer $fu_conformidad
 * @property integer $confiabilidad
 * @property integer $co_madurez
 * @property integer $co_tol_error
 * @property integer $co_recuperabil
 * @property integer $usabilidad
 * @property integer $us_entendimiento
 * @property integer $us_aprendizaje
 * @property integer $us_operabilidad
 * @property integer $us_atraccion
 * @property integer $eficiencia
 * @property integer $efi_comport_tiemp
 * @property integer $efi_util_recur
 * @property integer $cap_manten
 * @property integer $cm_cap_analiz
 * @property integer $cm_cambialidad
 * @property integer $cm_estabilidad
 * @property integer $cm_fac_prueba
 * @property integer $portabilidad
 * @property integer $po_adaptabilidad
 * @property integer $po_fac_instal
 * @property integer $po_reemplazabi
 * @property integer $po_coexistencia
 * @property integer $cal_enuso
 * @property integer $c_uso_eficacia
 * @property integer $c_uso_productividad
 * @property integer $c_uso_seguridad
 * @property integer $calidadTotal
 * @property integer $errorFound
 * @property integer $errorCorrect
 * @property string $errorDescrip
 *
 * @property Sistemas $fkSistema
 */
class Evaluaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $total_funcionalidad;
    public $total_confiabilidad;
    public $total_usabilidad;
    public $total_eficiencia;
    public $total_cap_manten;
    public $total_portabilidad;
    public $total_cal_enuso;
    public $analista;

    public static function tableName()
    {
        return 'evaluaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fk_sistema', 'funcionalidad', 'fu_adecuacion', 'fu_exactitud', 'fu_interoperabil', 'fu_conformidad', 'confiabilidad', 'co_madurez', 'co_tol_error', 'co_recuperabil', 'usabilidad', 'us_entendimiento', 'us_aprendizaje', 'us_operabilidad', 'us_atraccion', 'eficiencia', 'efi_comport_tiemp', 'efi_util_recur', 'cap_manten', 'cm_cap_analiz', 'cm_cambialidad', 'cm_estabilidad', 'cm_fac_prueba', 'portabilidad', 'po_adaptabilidad', 'po_fac_instal', 'po_reemplazabi', 'po_coexistencia', 'cal_enuso', 'c_uso_eficacia', 'c_uso_productividad', 'c_uso_seguridad', 'calidadTotal', 'errorFound', 'errorCorrect'], 'integer'],
            [['funcionalidad', 'fu_adecuacion', 'fu_exactitud', 'fu_interoperabil', 'fu_conformidad', 'confiabilidad', 'co_madurez', 'co_tol_error', 'co_recuperabil', 'usabilidad', 'us_entendimiento', 'us_aprendizaje', 'us_operabilidad', 'us_atraccion', 'eficiencia', 'efi_comport_tiemp', 'efi_util_recur', 'cap_manten', 'cm_cap_analiz', 'cm_cambialidad', 'cm_estabilidad', 'cm_fac_prueba', 'portabilidad', 'po_adaptabilidad', 'po_fac_instal', 'po_reemplazabi', 'po_coexistencia', 'cal_enuso', 'c_uso_eficacia', 'c_uso_productividad', 'c_uso_seguridad', 'calidadTotal','errorFound', 'errorCorrect','errorDescrip'], 'required'],
            [['errorDescrip'], 'string'],
            [['fk_sistema'], 'exist', 'skipOnError' => true, 'targetClass' => Sistemas::className(), 'targetAttribute' => ['fk_sistema' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'analista' => '',
            'funcionalidad' => 'Funcionalidad',
            'total_funcionalidad' => '',
            'fu_adecuacion' => 'Adecuación',
            'fu_exactitud' => 'Exactitud',
            'fu_interoperabil' => 'Interoperabilidad',
            'fu_conformidad' => 'Conformidad',
            'confiabilidad' => 'Confiabilidad',
            'total_confiabilidad' => '',
            'co_madurez' => 'Madurez',
            'co_tol_error' => 'Tolerancia a errores',
            'co_recuperabil' => 'Recuperabilidad',
            'usabilidad' => 'Usabilidad',
            'total_usabilidad' => '',
            'us_entendimiento' => 'Entendimiento',
            'us_aprendizaje' => 'Aprendizaje',
            'us_operabilidad' => 'Operabilidad',
            'us_atraccion' => 'Atracción',
            'eficiencia' => 'Eficiencia',
            'total_eficiencia' => '',
            'efi_comport_tiemp' => 'Comportamiento de tiempos',
            'efi_util_recur' => 'Utilización de recursos',
            'cap_manten' => 'Capacidad de Mantenimiento',
            'total_cap_manten' => '',
            'cm_cap_analiz' => 'Capacidad de ser analizado',
            'cm_cambialidad' => 'Cambialidad',
            'cm_estabilidad' => 'Estabilidad',
            'cm_fac_prueba' => 'Facilidad de Prueba',
            'portabilidad' => 'Portabilidad',
            'total_portabilidad' => '',
            'po_adaptabilidad' => 'Adaptabilidad',
            'po_fac_instal' => 'Facilidad de Instalacion',
            'po_reemplazabi' => 'Reemplazabilidad',
            'po_coexistencia' => 'Coexistencia',
            'cal_enuso' => 'Calidad en uso',
            'total_cal_enuso' => '',
            'c_uso_eficacia' => 'Eficacia',
            'c_uso_productividad' => 'Productividad',
            'c_uso_seguridad' => 'Seguridad',
            'calidadTotal' => 'Calidad del Software',
            'fk_sistema' => 'Nombre del sistema',
            'errorFound' => 'Errores Encontrados',
            'errorCorrect' => 'Errores Corregidos',
            'errorDescrip' => 'Descripción de Errores',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkSistema()
    {
        return $this->hasOne(Sistemas::className(), ['id' => 'fk_sistema']);
    }
    public function getNombreSistema()
    {
        $name = $this->fkSistema->nombre;
        return $name;
    }
}
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Analistas */

$this->title ="Analista: ".$model->nombre." ".$model->apellido;
$this->params['breadcrumbs'][] = ['label' => 'Analistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="analistas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p style="display: inline-block">
        <?= Html::a('Actualizar/Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta seguro de querer eliminar este Analista?',
                'method' => 'post',
            ],
        ]) ?>
<!--
    <form style="display: inline-block;margin-left: 0.3rem">
        <input class="btn btn-success" TYPE="Button" VALUE="Imprimir Reporte" ONCLICK="window.print()">
    </form>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'apellido',
            'cedula',
        ],
    ]) ?>

</div>

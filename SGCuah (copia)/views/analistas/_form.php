<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Analistas */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="analista">

    <div class="analistas-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'apellido')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'cedula')->textInput() ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
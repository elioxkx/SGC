<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Analistas */

$this->title = 'Editar Analista: ' . $model->nombre . $model->apellido;
$this->params['breadcrumbs'][] = ['label' => 'Analistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="analistas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

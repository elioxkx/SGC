
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use  \app\models\Analistas;

/* @var $this yii\web\View */
/* @var $model app\models\Evaluaciones */
$this->title = "Nombre: " . $model->fkSistema->nombre;
$analist= $model->fkSistema->fkAnalista->nombre;
/*$this->title = $model->id;*/
$this->params['breadcrumbs'][] = ['label' => 'Evaluaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="evaluaciones-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <h3 style="margin-left: 2%">Reporte General de Atributos Principales</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'fk_sistema',
                'value' => $model->fkSistema->nombre,
            ],
            [
                'attribute' => 'analista',
                'label' =>"Nombre del Analista de Calidad",
                'value' => $analist,
            ],
            'funcionalidad',
            'usabilidad',
            'eficiencia',
            'cap_manten',
            'portabilidad',
            'cal_enuso',
            'calidadTotal',
            'errorFound',
            'errorCorrect',
            'errorDescrip:ntext',
        ],
    ]) ?>
    <h3 style="margin-left: 2%">Reporte General de Sub-atributos </h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',[
                'attribute' => 'fk_sistema',
                'value' => $model->fkSistema->nombre,
            ],
            'fu_adecuacion',
            'fu_exactitud',
            'fu_interoperabil',
            'fu_conformidad',
            'co_madurez',
            'co_tol_error',
            'co_recuperabil',
            'us_entendimiento',
            'us_aprendizaje',
            'us_operabilidad',
            'us_atraccion',
            'efi_comport_tiemp',
            'efi_util_recur',
            'cm_cap_analiz',
            'cm_cambialidad',
            'cm_estabilidad',
            'cm_fac_prueba',
            'po_adaptabilidad',
            'po_fac_instal',
            'po_reemplazabi',
            'po_coexistencia',
            'c_uso_eficacia',
            'c_uso_productividad',
            'c_uso_seguridad',
            'calidadTotal',
        ],
    ]) ?>
    <p style="display: inline-block">
        <?= Html::a('Actualizar/Editar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

    <form style="display: inline-block;margin-left: 0.3rem">
        <input class="btn btn-success" TYPE="Button" VALUE="Imprimir Reporte" ONCLICK="window.print()">
    </form>
    </p>


</div>

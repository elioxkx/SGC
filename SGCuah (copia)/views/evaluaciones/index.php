<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EvaluacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evaluaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evaluaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Evaluaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'value' => 'NombreSistema',
                'label' => 'Sistema'
            ],
            'funcionalidad',
            //'fu_adecuacion',
            //'fu_exactitud',
            //'fu_interoperabil',
            // 'fu_conformidad',
            'confiabilidad',
            // 'co_madurez',
            // 'co_tol_error',
            // 'co_recuperabil',
            'usabilidad',
            // 'us_entendimiento',
            // 'us_aprendizaje',
            // 'us_operabilidad',
            // 'us_atraccion',
            'eficiencia',
            // 'efi_comport_tiemp',
            // 'efi_util_recur',
            'cap_manten',
            // 'cm_cap_analiz',
            // 'cm_cambialidad',
            // 'cm_estabilidad',
            // 'cm_fac_prueba',
            'portabilidad',
            // 'po_adaptabilidad',
            // 'po_fac_instal',
            // 'po_reemplazabi',
            // 'po_coexistencia',
            'cal_enuso',
            // 'c_uso_eficacia',
            // 'c_uso_productividad',
            // 'c_uso_seguridad',
            'calidadTotal',
            // 'fk_sistema',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

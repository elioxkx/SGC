<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron" style="margin-top: 6%">
        <h2>Bienvenido al Sistema de Evaluación de Calidad!</h2>

        <p class="lead">Para realizar una nueva evaluación presione aquí</p>

        <p>
            <?= Html::a('Nueva Evaluación &raquo;', ['evaluaciones/create'], ['class' => 'btn btn-lg btn-success']) ?>
        </p>
    </div>

    <div class="body-content">
        <h3 class="text-center">Si es primeva vez que ingresas, debes registrar un Analista y posteriormente el Sistema a evaluar.</h3>

        <div class="row">
            <div class="col-lg-4">
                <h2 class="h2tag">Analista</h2>

                <p class="text-center">Registro de nuevos analistas.</p>

                <p class="text-center"><a class="btn btn-success" href="http://localhost:8080/analistas/create">Nuevo Analista &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2 class="h2tag">Sistema</h2>

                <p class="text-center">Registro de nuevo Sistema.</p>

                <p class="text-center"><a class="btn btn-success" href="http://localhost:8080/sistemas/create">Nuevo Sistema &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2 class="h2tag">Reportes</h2>

                <p class="text-center">Reportes de Evaluaciones ya realizadas.</p>

                <p class="text-center"><a class="btn btn-success" href="http://localhost:8080/evaluaciones/index"> Visualizar &raquo;</a></p>
            </div>
        </div>

    </div>
</div>

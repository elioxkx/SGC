<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sistemas */
/* @var $form yii\widgets\ActiveForm */
/*$name = $model->fkAnalista->nombre. " "$model->fkAnalista->apellido;*/
?>

<div class="sistemas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

<!--    <?/*= $form->field($model, 'fk_analista')->textInput() */?>-->
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'fk_analista')->dropDownList(
                \yii\helpers\ArrayHelper::map(\app\models\Analistas::find()->all(),'id','nombre', 'apellido')) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Editar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

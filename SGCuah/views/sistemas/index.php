<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SistemasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sistemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sistemas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sistemas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'descripcion:ntext',
            'fk_analista',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

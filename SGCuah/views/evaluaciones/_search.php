<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EvaluacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evaluaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'funcionalidad') ?>

    <?= $form->field($model, 'fu_adecuacion') ?>

    <?= $form->field($model, 'fu_exactitud') ?>

    <?= $form->field($model, 'fu_interoperabil') ?>

    <?php // echo $form->field($model, 'fu_conformidad') ?>

    <?php // echo $form->field($model, 'confiabilidad') ?>

    <?php // echo $form->field($model, 'co_madurez') ?>

    <?php // echo $form->field($model, 'co_tol_error') ?>

    <?php // echo $form->field($model, 'co_recuperabil') ?>

    <?php // echo $form->field($model, 'usabilidad') ?>

    <?php // echo $form->field($model, 'us_entendimiento') ?>

    <?php // echo $form->field($model, 'us_aprendizaje') ?>

    <?php // echo $form->field($model, 'us_operabilidad') ?>

    <?php // echo $form->field($model, 'us_atraccion') ?>

    <?php // echo $form->field($model, 'eficiencia') ?>

    <?php // echo $form->field($model, 'efi_comport_tiemp') ?>

    <?php // echo $form->field($model, 'efi_util_recur') ?>

    <?php // echo $form->field($model, 'cap_manten') ?>

    <?php // echo $form->field($model, 'cm_cap_analiz') ?>

    <?php // echo $form->field($model, 'cm_cambialidad') ?>

    <?php // echo $form->field($model, 'cm_estabilidad') ?>

    <?php // echo $form->field($model, 'cm_fac_prueba') ?>

    <?php // echo $form->field($model, 'portabilidad') ?>

    <?php // echo $form->field($model, 'po_adaptabilidad') ?>

    <?php // echo $form->field($model, 'po_fac_instal') ?>

    <?php // echo $form->field($model, 'po_reemplazabi') ?>

    <?php // echo $form->field($model, 'po_coexistencia') ?>

    <?php // echo $form->field($model, 'cal_enuso') ?>

    <?php // echo $form->field($model, 'c_uso_eficacia') ?>

    <?php // echo $form->field($model, 'c_uso_productividad') ?>

    <?php // echo $form->field($model, 'c_uso_seguridad') ?>

    <?php // echo $form->field($model, 'calidadTotal') ?>

    <?php // echo $form->field($model, 'fk_sistema') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

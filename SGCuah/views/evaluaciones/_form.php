<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile(Yii::$app->request->baseUrl . '/js/calculos.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

/* @var $this yii\web\View */
/* @var $model app\models\Evaluaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evaluaciones-form container-fluid">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'fk_sistema')->dropDownList(
                \yii\helpers\ArrayHelper::map(\app\models\Sistemas::find()->all(),'id','nombre')) ?>
        </div>
    </div>

    <div class="row">
        <!--FUNCIONALIDAD-->
        <div class="col-md-12 askbox">
            <div class="col-md-6">
                <div name="funcionalidad" style="margin-top: 25%">
                    <p>¿Las funciones y Propiedades satisfacen las necesidades Explícitas e implícitas; esto es, el qué?</p>
                    <hr>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_funcionalidad')->checkbox(); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'funcionalidad')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿Tiene el conjunto de funciones apropiadas para las tareas especificadas?</p>
                <div class="">
                    <?= $form->field($model, 'fu_adecuacion')->checkbox(['class' => 'funcionalidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Hace lo que fue acordado en forma esperada y correcta?</p>
                <div class="">
                    <?= $form->field($model, 'fu_exactitud')->checkbox(['class' => 'funcionalidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Interactúa con otros sistemas especificados?</p>
                <div class="">
                    <?= $form->field($model, 'fu_interoperabil')->checkbox(['class' => 'funcionalidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Está de acuerdo con las leyes o normas y estándares, u otras prescripciones?</p>
                <div class="">
                    <?= $form->field($model, 'fu_conformidad')->checkbox(['class' => 'funcionalidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
            </div>
        </div>
        <!--/FUNCIONALIDAD-->

        <!--CONFIABILIDAD-->
        <div class="col-md-12 askbox">
            <div class="col-md-6">
                <div style="margin-top: 20%">
                    <p>¿Puede mantener el
                        nivel de rendimiento,
                        bajo ciertas condiciones
                        y por cierto tiempo?
                    </p>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_confiabilidad')->checkbox(/*array('disabled' => true)*/); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'confiabilidad')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿Con qué frecuencia presenta fallas por defectos o errores?</p>
                <div class="">
                    <?= $form->field($model, 'co_madurez')->checkbox(['class' => 'confiabilidad_check', 'value' => 33]) ?>
                    <hr>
                </div>
                <p>¿Si suceden fallas, como se
                    comporta en cuanto a la
                    performancia especificada?</p>
                <div class="">
                    <?= $form->field($model, 'co_tol_error')->checkbox(['class' => 'confiabilidad_check', 'value' => 34]) ?>
                    <hr>
                </div>
                <p>¿Es capaz de recuperar datos en
                    caso de fallas?</p>
                <div class="">
                    <?= $form->field($model, 'co_recuperabil')->checkbox(['class' => 'confiabilidad_check', 'value' => 33]) ?>
                    <hr>
                </div>
            </div>
            <hr>
        </div>
        <!--/CONFIABILIDAD-->

        <!--USABILIDAD-->
        <div class="col-md-12 askbox">
            <div class="col-md-6">
                <div style="margin-top: 28%">
                    <p>¿El software, es fácil de usar y de aprender?</p>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_usabilidad')->checkbox(/*array('disabled' => true)*/); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'usabilidad')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿Es fácil de entender y reconocer la estructura y la lógica y su aplicabilidad?</p>
                <div class="">
                    <?= $form->field($model, 'us_entendimiento')->checkbox(['class' => 'usabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Es fácil de aprender a usar?</p>
                <div class="">
                    <?= $form->field($model, 'us_aprendizaje')->checkbox(['class' => 'usabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Es fácil de operar y controlar?</p>
                <div class="">
                    <?= $form->field($model, 'us_operabilidad')->checkbox(['class' => 'usabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Es atractivo el diseño del software?</p>
                <div class="">
                    <?= $form->field($model, 'us_atraccion')->checkbox(['class' => 'usabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
            </div>
            <hr>
        </div>
        <!--/USABILIDAD-->

        <!--EFICIENCIA-->
        <div class="col-md-12 askbox">
            <div class="col-md-6 ">
                <div style="margin-top: 12%">
                    <p>¿Es rápido y minimalista en cuanto a uso de recursos, bajo ciertas condiciones?</p>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_eficiencia')->checkbox(/*array('disabled' => true)*/); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'eficiencia')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿Cuál es el tiempo de respuesta
                    y performancia en la ejecución
                    de la función?</p>
                <div class="">
                    <?= $form->field($model, 'efi_comport_tiemp')->checkbox(['class' => 'eficiencia_check', 'value' => 50]) ?>
                    <hr>
                </div>
                <p>¿Cuántos recursos usa y durante
                    cuánto tiempo?</p>
                <div class="">
                    <?= $form->field($model, 'efi_util_recur')->checkbox(['class' => 'eficiencia_check', 'value' => 50]) ?>
                    <hr>
                </div>
            </div>
            <hr>
        </div>
        <!--/EFICIENCIA-->
        <hr>
        <!--CAPACIDAD DE MANTENIMIENTO-->
        <div class="col-md-12 askbox">
            <div class="col-md-6">
                <div style="margin-top: 28%">
                    <p>¿Es fácil de modificar y testear?</p>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_cap_manten')->checkbox(/*array('disabled' => true)*/); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'cap_manten')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿Es fácil diagnosticar una falla o identificar partes a modificar?</p>
                <div class="">
                    <?= $form->field($model, 'cm_cap_analiz')->checkbox(['class' => 'cap_manten_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Es fácil de modificar y adaptar?</p>
                <div class="">
                    <?= $form->field($model, 'cm_cambialidad')->checkbox(['class' => 'cap_manten_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Hay riesgos o efectos inesperados cuando se realizan cambios?</p>
                <div class="">
                    <?= $form->field($model, 'cm_estabilidad')->checkbox(['class' => 'cap_manten_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Son fáciles de validar las modificaciones? </p>
                <div class="">
                    <?= $form->field($model, 'cm_fac_prueba')->checkbox(['class' => 'cap_manten_check', 'value' => 25]) ?>
                    <hr>
                </div>
            </div>
            <hr>
        </div>
        <!--/CAPACIDAD DE MANTENIMIENTO-->

        <!--PORTABILIDAD-->
        <div class="col-md-12 askbox">
            <div class="col-md-6">
                <div style="margin-top: 29%">
                    <p>¿Es fácil de transferir de un ambiente a otro?</p>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_portabilidad')->checkbox(/*array('disabled' => true)*/); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'portabilidad')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿Es fácil de adaptar a otros entornos con lo provisto?</p>
                <div class="">
                    <?= $form->field($model, 'po_adaptabilidad')->checkbox(['class' => 'portabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Es fácil de instalar en el ambiente especificado?</p>
                <div class="">
                    <?= $form->field($model, 'po_fac_instal')->checkbox(['class' => 'portabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Es fácil de usarlo en lugar de otro software para ese ambiente?</p>
                <div class="">
                    <?= $form->field($model, 'po_reemplazabi')->checkbox(['class' => 'portabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
                <p>¿Comparte sin dificultad recursos con otro software o dispositivo?</p>
                <div class="">
                    <?= $form->field($model, 'po_coexistencia')->checkbox(['class' => 'portabilidad_check', 'value' => 25]) ?>
                    <hr>
                </div>
            </div>
            <hr>
        </div>
        <!--/PORTABILIDAD-->

        <!--CALIDAD EN USO-->
        <div class="col-md-12 askbox">
            <div class="col-md-6">
                <div style="margin-top: 20%">
                    <p>¿Muestra el usuario final aceptación y seguridad del software?</p>
                    <div class="insideBox">
                        <?= $form->field($model, 'total_cal_enuso')->checkbox(/*array('disabled' => true)*/); ?>
                    </div>
                    <div class="insideBox">
                        <?= $form->field($model, 'cal_enuso')->textInput(['readonly' => true]); ?>
                    </div>
                </div>
                <hr>
            </div>
            <div class="col-md-6 leftLine">
                <p>¿La eficaz el software cuando el usuario final realiza los procesos?</p>
                <div class="">
                    <?= $form->field($model, 'c_uso_eficacia')->checkbox(['class' => 'cal_enuso_check', 'value' => 33]) ?>
                    <hr>
                </div>
                <p>¿Muestra el usuario final rendimiento en sus tareas cotidianas del proceso específico?</p>
                <div class="">
                    <?= $form->field($model, 'c_uso_productividad')->checkbox(['class' => 'cal_enuso_check', 'value' => 33]) ?>
                    <hr>
                </div>
                <p>¿El software tiene niveles de riesgo que causan daño al usuario final?</p>
                <div class="">
                    <?= $form->field($model, 'c_uso_seguridad')->checkbox(['class' => 'cal_enuso_check', 'value' => 34]) ?>
                    <hr>
                </div>
            </div>
            <hr>
        </div>
        <!--/CALIDAD EN USO-->
    </div>
    <?= $form->field($model, 'calidadTotal')->textInput(['readonly' => true,'class' => 'calidadTotal',]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Analistas */

$this->title = 'Create Analistas';
$this->params['breadcrumbs'][] = ['label' => 'Analistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="analistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

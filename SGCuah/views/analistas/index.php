<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnalistasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Analistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="analistas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Analistas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellido',
            'cedula',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

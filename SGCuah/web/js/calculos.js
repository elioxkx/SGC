/**
 * Created by elio on 4/7/16.
 */
/*--------------------FUNCIONALIDAD--------------------*/
funcionalidad = 0;
confiabilidad = 0;
usabilidad = 0;
eficiencia = 0;
cap_manten = 0;
portabilidad = 0;
cal_enuso = 0;

$(document).on(' change', 'input[id="evaluaciones-total_funcionalidad"]', function () {
    if ($('.funcionalidad_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("funcionalidad_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-funcionalidad").val(count);
        funcionalidad = parseInt($("#evaluaciones-funcionalidad").val());
        suma();
    }
});

$(".funcionalidad_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("funcionalidad_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }
    /* alert(count);*/
    $("#evaluaciones-funcionalidad").val(count);
    funcionalidad = parseInt($("#evaluaciones-funcionalidad").val());
    suma();
});
/*--------------------FUNCIONALIDAD--------------------*/

/*--------------------CONFIABILIDAD--------------------*/
$(document).on(' change', 'input[id="evaluaciones-total_confiabilidad"]', function () {
    if ($('.confiabilidad_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("confiabilidad_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-confiabilidad").val(count);
        confiabilidad = parseInt($("#evaluaciones-confiabilidad").val());
        suma();
    }
});
$(".confiabilidad_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("confiabilidad_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }
    /* alert(count);*/
    $("#evaluaciones-confiabilidad").val(count);
    confiabilidad = parseInt($("#evaluaciones-confiabilidad").val());
    suma();
});
/*--------------------/CONFIABILIDAD--------------------*/


/*--------------------USABILIDAD--------------------*/
$(document).on(' change', 'input[id="evaluaciones-total_usabilidad"]', function () {
    if ($('.usabilidad_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("usabilidad_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-usabilidad").val(count);
        usabilidad = parseInt($("#evaluaciones-usabilidad").val());
        suma();
    }
});
$(".usabilidad_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("usabilidad_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }
    /* alert(count);*/
    $("#evaluaciones-usabilidad").val(count);
    usabilidad = parseInt($("#evaluaciones-usabilidad").val());
    suma();
});
/*--------------------/USABILIDAD--------------------*/

/*--------------------EFICIENCIA--------------------*/
$(document).on(' change', 'input[id="evaluaciones-total_eficiencia"]', function () {
    if ($('.eficiencia_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("eficiencia_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-eficiencia").val(count);
        eficiencia = parseInt($("#evaluaciones-eficiencia").val());
        suma();
    }
});
$(".eficiencia_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("eficiencia_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }
    /* alert(count);*/
    $("#evaluaciones-eficiencia").val(count);
    eficiencia = parseInt($("#evaluaciones-eficiencia").val());
    suma();
});
/*--------------------/EFICIENCIA--------------------*/

/*--------------------CAPACIDAD DE MANTENIMIENTO--------------------*/
$(document).on(' change', 'input[id="evaluaciones-total_cap_manten"]', function () {
    if ($('.cap_manten_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("cap_manten_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-cap_manten").val(count);
        cap_manten = parseInt($("#evaluaciones-cap_manten").val());
        suma();
    }
});
$(".cap_manten_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("cap_manten_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }
    /* alert(count);*/
    $("#evaluaciones-cap_manten").val(count);
    cap_manten = parseInt($("#evaluaciones-cap_manten").val());
    suma();
});
/*--------------------/CAPACIDAD DE MANTENIMIENTO--------------------*/

/*--------------------PORTABILIDAD--------------------*/
$(document).on(' change', 'input[id="evaluaciones-total_portabilidad"]', function () {
    if ($('.portabilidad_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("portabilidad_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-portabilidad").val(count);
        portabilidad = parseInt($("#evaluaciones-portabilidad").val());
        suma();
    }
});
$(".portabilidad_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("portabilidad_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }
    /* alert(count);*/
    $("#evaluaciones-portabilidad").val(count);
    portabilidad = parseInt($("#evaluaciones-portabilidad").val());
    suma();
});
/*--------------------/PORTABILIDAD--------------------*/

/*--------------------CALIDAD EN USO--------------------*/
$(document).on(' change', 'input[id="evaluaciones-total_cal_enuso"]', function () {
    if ($('.cal_enuso_check').prop("checked", this.checked)) {
        var count = 0;
        var table_abc = document.getElementsByClassName("cal_enuso_check");
        for (var i = 0; table_abc[i]; ++i) {

            if (table_abc[i].checked) {
                var value = table_abc[i].value;
                count += Number(table_abc[i].value);
            }
        }
        /*alert(count);*/
        $("#evaluaciones-cal_enuso").val(count);
        cal_enuso = parseInt($("#evaluaciones-cal_enuso").val());
        suma();
    }
});
$(".cal_enuso_check").change(function () {
    var count = 0;
    var table_abc = document.getElementsByClassName("cal_enuso_check");
    for (var i = 0; table_abc[i]; ++i) {

        if (table_abc[i].checked) {
            var value = table_abc[i].value;
            count += Number(table_abc[i].value);
        }
    }

    $("#evaluaciones-cal_enuso").val(count);
    cal_enuso = parseInt($("#evaluaciones-cal_enuso").val());
    suma();
});

/*--------------------/CALIDAD EN USO--------------------*/

/*--------------------/CALIDAD TOTAL--------------------*/

function suma() {
    setTimeout(function () {
        var total = Math.round( (funcionalidad + confiabilidad + usabilidad + eficiencia + cap_manten + portabilidad + cal_enuso)/7);
        $("#evaluaciones-calidadtotal").val(total);
    }, 500);
}

/*--------------------/CALIDAD TOTAL--------------------*/

$(document).bind("keyup keydown", function(e){
    if(e.ctrlKey && e.keyCode == 80){
        alert("hola toy");
    }
});

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sistemas".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property integer $fk_analista
 *
 * @property Evaluaciones[] $evaluaciones
 * @property Responsables[] $responsables
 * @property Analistas $fkAnalista
 */
class Sistemas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sistemas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string'],
            [['fk_analista'], 'integer'],
            [['nombre'], 'string', 'max' => 255],
            [['fk_analista'], 'exist', 'skipOnError' => true, 'targetClass' => Analistas::className(), 'targetAttribute' => ['fk_analista' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fk_analista' => 'Fk Analista',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvaluaciones()
    {
        return $this->hasMany(Evaluaciones::className(), ['fk_sistema' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsables()
    {
        return $this->hasMany(Responsables::className(), ['fk_sistema' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFkAnalista()
    {
        return $this->hasOne(Analistas::className(), ['id' => 'fk_analista']);
    }
}

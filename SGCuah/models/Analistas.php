<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "analistas".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property integer $cedula
 *
 * @property Sistemas[] $sistemas
 */
class Analistas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'analistas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido'], 'required'],
            [['cedula'], 'integer'],
            [['nombre', 'apellido'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'cedula' => 'Cedula',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSistemas()
    {
        return $this->hasMany(Sistemas::className(), ['fk_analista' => 'id']);
    }
}

<?php

use yii\db\Migration;

class m160405_211048_analistas extends Migration
{
    public function up()
    {
        $this->createTable('analistas', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'apellido' => $this->string()->notNull(),
            'cedula' => $this->integer()
        ]);
    }

    public function down()
    {
        /*echo "m160405_183221_analistas cannot be reverted.\n";

        return false;*/
        $this->dropTable('analistas');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
